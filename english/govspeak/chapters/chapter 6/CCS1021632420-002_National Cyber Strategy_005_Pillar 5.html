##Pillar 5: Countering Threats

(image)

###Detecting, disrupting and deterring our adversaries to enhance UK security in and through cyberspace

The nature of the threat we face is complex. We are concerned about threats in cyberspace (for example to our online activities), threats to the UK and partners through cyberspace (for example to networked UK critical national infrastructure), and threats to the functioning of underpinning international cyber infrastructure. All of these threats can impact the availability of services that people rely on, or the confidentiality or integrity of data and information that passes through those systems. The foundations of our approach to countering the threat are in promoting cyber resilience as outlined earlier in this document. This chapter focuses on how we will raise the costs and risks of attacking the UK in cyberspace and ensure we achieve our full potential as a cyber power.

Since the National Cyber Security Strategy 2016-2021, we have transformed our approach to mitigating the threat. We have established world class cyber threat detection and analysis capabilities as part of the National Cyber Security Centre (NCSC). The NCSC works with partners across the public and private sector, at home and overseas, to detect and respond to threats and incidents. As part of the wider intelligence community the NCSC has also been able to inform policy makers on the attribution of attacks against UK interests, which is a critical part of our approach to deterring cyber threats. We have significantly invested in our offensive cyber capabilities, through the National Offensive Cyber Programme and now the new National Cyber Force (NCF). We have also developed an integrated National Crime Agency (NCA)-led national law enforcement response and have sought to disrupt and raise the cost of hostile and criminal activity in cyberspace. We have created world class threat detection and assessment capabilities with the means to translate the resultant insight into impactful mitigations across the public and private sector. And we have developed an autonomous cyber sanctions regime as another lever to impose costs on hostile actors. In combination, our diplomatic engagement, the NCSC, our security and intelligence agencies, the NCA, wider law enforcement and the NCF have reduced the real-world impact caused by threats, by taking action to directly counter adversaries, help prevent attacks, and reduce harm.

But the threats have also grown in sophistication, complexity and severity; and our efforts have not yet fundamentally altered the risk calculus of attackers who continue to successfully target the UK and its interests. Cyber attacks against the UK are motivated by espionage, criminal, commercial, financial and political gain, sabotage and disinformation. Attackers develop capabilities that evade mitigations; increasingly sophisticated cyber tools and related enablers have been commoditised in a growing industry, lowering the barriers to entry for all types of malicious actors. And rewards are increasing as the ability of actors to steal and encrypt valuable data and extort ransomware payments continues to grow, disrupting businesses and key public services. The result has seen attackers increasingly benefit financially, exploit privacy and freedom of speech, and attempt to manipulate events through disinformation.

The UK’s approach will therefore now shift to a more integrated and sustained campaign footing that will involve making routine, integrated and creative use of the full range of levers and capabilities available to impose costs on our adversaries, pursue and disrupt perpetrators and deter future attacks. <strong>The key supporting elements of this approach will be: </strong>

- the continued development of the NCF as the next step in the UK’s ability to conduct offensive cyber operations against its adversaries

- tailored cross-government campaigns to tackle threats to the UK – making use of our diplomatic, military, intelligence, law enforcement, economic, legal and strategic communications tools

- new investment to enable law enforcement to pursue investigations at scale and pace and to maintain a technical edge over our adversaries in order to prevent and detect serious criminals and the enabling services they rely upon

- a major step up in data sharing across government and industry as outlined in the Resilience chapter

Cyberspace presents opportunities for the UK, creating new ways to actively pursue our national interests. For example, offensive cyber operations offer us a range of flexible, scalable, and de-escalatory measures that will help the UK to maintain our strategic advantage and to deliver national priorities, often in ways that avoid the need to put individuals at risk of physical harm. 

We will continue to develop and invest in our offensive cyber capabilities, through the NCF. The NCF will transform the UK’s ability to contest adversaries in cyber space and the real world, to protect the country, its people and our way of life. These capabilities will be used responsibly as a force for good alongside diplomatic, economic, criminal justice and military levers of power. They will be used to support and advance a wide range of government priorities relating to national security, economic wellbeing, and in support of the prevention and detection of serious crime. 

####Objective 1: Detect, investigate and share information on state, criminal and other malicious cyber actors and activities in order to protect the UK, its interests and its citizens

<strong>We will achieve the following outcomes by 2025:</strong>

<strong>The government has a comprehensive understanding of the cyber capabilities of state, criminal and other malicious cyber actors and their strategic intent towards the UK.</strong> We will sustain and grow the considerable investments made under the 2016 strategy in the intelligence agencies and law enforcement for understanding the cyber threat. In particular, we will increase law enforcement’s capability to understand and tackle the cyber crime threat, including its links to state and other international and domestic threats and its technological enablers, helping us to develop more effective policy responses. We will improve how we coordinate threat detection across government, with a joint data access and exploitation strategy across the intelligence agencies and law enforcement. And we will focus even more on understanding the intent and decision-making criteria of our adversaries and the impact our activities are having on them, including how individuals become cyber criminals and what action we can take to prevent this from happening. 

Our work to enable faster and easier reporting of cyber incidents and crimes, outlined in the Resilience chapter will also help to achieve this outcome. 

<strong>Most serious state, criminal and other threats are routinely and comprehensively investigated</strong>, drawing on all sources of information and bringing together expertise across government, law enforcement and the private sector. We will build the intelligence, operational and technical capabilities of the UK’s law enforcement cyber network. We will invest in the NCA’s cyber intelligence capability, used to target organised crime groups, the regional intelligence build initiative, which will enhance intelligence access and movement across the UK, and the skills and capability that law enforcement need to investigate and disrupt cyber and digital crimes. 

Investigations will be supported by intelligence from all sources and leverage skills and knowledge across the private sector, including by helping businesses to share data more easily with law enforcement. And we will continue to implement the recommendations of the HMICFRS on the policing response to cyber crime to ensure the cyber crime network at the national, regional and local levels remains on a secure footing.<a href="CCS1021632420-002_National_Cyber_Strategy_005_Pillar_5.html#footnote-003">34</a>

<strong>Information and data on the threat is routinely shared at scale and pace and those who receive it are more able to take action</strong>. The NCSC has trialled a range of initiatives to build up more effective communities of network defenders, across a wide variety of sectors, who not only receive and are able to share threat information, but are increasingly capable at using it for collective benefit. We will expand this work, with an initial focus on helping government defend itself better, supported by the Government Cyber Coordination Centre (described in the Resilience chapter). The Financial Sector Cyber Collaboration Centre is already leading the way in the private sector.<a href="CCS1021632420-002_National_Cyber_Strategy_005_Pillar_5.html#footnote-002">35</a>

The NCSC are also investigating ways to track emerging threats and continue to work with the Alan Turing Institute to explore whether machine learning can be used to detect certain types of cyber attack. This research will continue to improve our understanding of how we can use artificial intelligence to detect malicious activity.

###Stopping cyber crime also means tackling other types of criminal activity

Cyber crimes (defined as Computer Misuse Act offences) occur when there is unauthorised access to computers, networks, data and other digital devices, associated acts that cause damage or the making or supplying of tools to commit these offences. This can allow cyber criminals to commit further malicious cyber activities such as ransomware attacks, unauthorised account access, intellectual property theft, denial of service attacks, or the stealing of large personal data sets – these are significant and growing crimes. 

For citizens, cyber crimes often manifest in the further crimes they enable and facilitate. Unauthorised computer access can lead to a wide range of frauds, theft, sextortion, and in some cases facilitate stalking, domestic abuse and harassment. All of these crimes cause significant harm to UK citizens on a daily basis, destroying businesses and ruining lives. Cyber crimes are therefore distinct and different from wider online safety issues such as bullying and harassment, hate speech, the spreading of disinformation, promotion of gang culture and violence, or underage access to pornography. The government is addressing these issues through the online harms white paper and draft online safety bill.

###Objective 2: Deter and disrupt state, criminal and other malicious cyber actors and activities against the UK, its interests, and its citizens.

<strong>We will achieve the following outcomes by 2025:</strong>

<strong>It is more costly and higher risk for state, criminal and other malicious cyber actors to target the UK</strong>. We will implement sustained and tailored deterrence campaigns that leverage the full range of UK capabilities (including diplomatic, economic, covert and overt levers) to influence the behaviour of malicious and criminal cyber actors. In particular we will improve our signalling to adversaries of our capability and willingness to impose meaningful costs, including through sanctions, law enforcement and NCF operations. And through NCA’s Cyber Choices programme we will divert individuals from becoming involved in cyber crime, working with industry and academia to offer potential offenders better alternatives such as apprenticeships and work placements.

We will also provide the tools and powers law enforcement and the intelligence agencies need through the Counter State Threats Bill, by updating existing legislation and introducing new offences to account for how state threats have evolved. And we will amend the Proceeds of Crime Act 2002 to optimise law enforcement’s ability to identify, seize and recover the proceeds of cyber crime. In particular, we will do this by creating a civil forfeiture power to mitigate the risks posed by those who cannot be prosecuted.

<strong>State, criminal and other malicious cyber actors are less able to target the UK</strong> as a result of our disruption and denigration of their activities and capabilities. We will review the government’s policy and operational approach to tackling ransomware, adopting this as one of our priority campaigns, collaborating with industry and our international partners. We will maximise the partnerships between the NCF, NCSC, NCA and wider law enforcement and the diplomatic and intelligence communities to counter threats that disrupt the confidentiality, integrity and availability of cyberspace or data and services in cyberspace. In particular we will invest in capabilities to target cybercriminal infrastructure and deploy our law enforcement and offensive cyber capabilities to disrupt malicious cyber activity. Our adversaries are building cyber capabilities and increasingly using them for malign purposes. We will make full use of the NCF, where we deem it appropriate, to disrupt these efforts and defend and protect the UK.

We will also counter the proliferation of high-end cyber capabilities to states and organised crime groups via commercial and criminal marketplaces, tackling forums that enable, facilitate, or glamorise cyber criminality. 

<strong>An increase in criminal justice and other disruptive outcomes for cyber criminals</strong> with improved criminal justice capability used to prosecute cyber criminals in the UK. We will review the Computer Misuse Act (CMA) and relevant powers to ensure that law enforcement agencies have the ability to investigate new and emerging threats from criminals and introduce more specialist prosecutors to deal with the increasing number of cyber cases. We will also improve specialist law enforcement skills, exercising and mainstreaming to ensure a continuing supply of officers with required cyber specialist knowledge, through the National Police Chiefs’ Council (NPCC) cyber skills prospectus and the College of Policing Cyber Digital Career Pathways. 

<strong>Susan Moody, Police Service of Northern Ireland (PSNI) Prevent Officer</strong>

L-R Susan Moody (PSNI), Sarah Travers (TV presenter) and Joe Dolan (Head of the Northern Ireland Cyber Security Centre)

Computers and mobile devices are part of a young person’s everyday life. They provide great opportunities but also dangers if misused. The prevent function within PSNI provides a much needed early intervention with young people and helps them understand the law around using and misusing computers. This highlights the danger signs for potential criminal activity and also highlights the great opportunities through initiatives such as CyberFirst and cyber as a career, which can give those who have a curiosity or talent an alternative to offending, and prevent abuse by others for criminal ends. Susan has worked tirelessly in developing a schools cyber information programme available for all secondary schools and has engaged directly with more than 40 primary schools, numerous secondary schools, young people’s organisations and uniformed groups. These young people could well be our cyber ambassadors and defenders of the future.

####Objective 3: Take action in and through cyberspace to support our national security and the prevention and detection of serious crime

<strong>We will achieve the following outcomes by 2025:</strong>

<strong>The UK’s cyber capabilities have a greater impact in deterring and disrupting non-cyber threats</strong>. We will scale-up and develop the NCF, delivering on our long-term vision for this key capability, ensuring that it is fully integrated with GCHQ, MOD, SIS and Dstl and working closely with law enforcement and wider government. We will deliver legal and proportionate offensive cyber operations through the NCF, acting responsibly in cyberspace and leading by example. Offensive cyber operations will continue to support the UK’s national security, including our defence and foreign policy, and in the prevention of serious crime. 

- We will also scale up and develop law enforcement technical capabilities against infrastructure and cryptocurrency, which can be deployed against other threats.

<strong>UK cyber capabilities are integrated across the full breadth of defence operations</strong> in line with the Integrated Operating Concept 2025.<a href="CCS1021632420-002_National_Cyber_Strategy_005_Pillar_5.html#footnote-001">36</a> This will maintain our competitive warfighting edge over adversaries and enable greater collaboration with our allies and partners. We will continue to progress the Defence Multi-Domain Integration Change Programme, which will bring together capabilities across the domains, and deliver greater integration with other instruments of our national power, bolstering our military advantage over our adversaries. Cyber will be a mainstream part of defence business enabled by highly skilled cyber specialists, an overall cyber awareness across the defence workforce and cutting-edge and resilient cyber capabilities.

<strong>Major law enforcement cyber crime investigations </strong>

<strong>Operation Imperil:</strong> Op Imperil was a joint South East Regional Organised Crime Unit (SEROCU) investigation with the FBI into a website selling compromised personal and banking information of victims of cyber-attacks. This enabled others to purchase personal data to commit frauds and further computer misuse offences. Significant investigation led to the identification of bank accounts and payments used for the technical infrastructure, identifying that the website owner was based in Pakistan. This enabled the FBI to covertly seize the website and then subsequently take it down. The South East Regional Organised Crime Unit arrested the primary UK suspect who was discovered to have opened a US based bank account on behalf of the website owner for the laundering of criminal funds. The UK suspect committed significant fraud using some of the compromised victim data, opening bank accounts in other names, utilising compromised bank accounts to pay for luxury holidays and false Department of Work and Pensions claims resulting in a loss to the state of in excess of £90,000. The suspect was charged with nine counts and sentenced to four years imprisonment reduced due to an early guilty plea. The judge awarded the investigative team a Judge’s Commendation. Confiscation and a lifetime Proceeds Of Crime Act application is in progress at time of publication.

<strong>Operation Nipigon:</strong> This was a Met Police investigation into a Bulgarian National who was suspected of creating bespoke phishing pages estimated to have caused losses to the UK in excess of £40 million. He was identified following an investigation into another well known cyber criminal previously sentenced to 10 years custodial in 2018, who was using the phishing pages created by the Bulgarian National to enable his own criminality. The investigation was opened following the identification of a significant email address linked to the suspect which after a number of protracted and complex enquiries led to engagement with the Bulgarian authorities and the suspect was arrested, extradited and following comprehensive disclosure, pleaded guilty to all criminal charges receiving a nine and a half year custodial sentence.

<strong>Operation Leasing:</strong> In 2020, the NCA led an investigation into terrorist bomb threats made against the NHS at the height of the COVID-19 pandemic, demanding payments in Bitcoin (BTC). Working with German authorities, the NCA identified and apprehended the suspect, who was successfully convicted in a German court.

On 12th April 2020, an Italian national, living in Germany, sent an email via the TOR network stating his intention to bomb an NHS hospital unless he received £10 million in Bitcoin.

This was rapidly identified as a high-priority by the NCA and specialist cyber crime officers were tasked to identify the perpetrator and prevent any potential attack.

The perpetrator had also sent emails threatening to attack MPs and bomb Black Lives Matter protestors in London. Despite writing his emails in English, NCA investigators used specialised cyber techniques and behavioural and linguistic analysis to deduce that the offender was likely a native German speaker.

In collaboration with German authorities, NCA officers identified that the emails were sent from a computer at an address in Berlin. Through international collaborative working, and despite significant efforts to mask his identity and location, the suspect was identified and placed under surveillance by German law enforcement. On 15 June 2020 the suspect was arrested, charged with attempted extortion and remanded in custody. He was convicted on 26 February 2021 and sentenced to three years in prison.

###Taking action through cyberspace to counter terrorism

<strong>Counter Daesh campaign:</strong> MOD and GCHQ’s work against Daesh is an example of how we have taken active steps to counter threats from those who misuse the power of the internet and modern communications. 

Daesh devoted much time and energy to technology, to create media content used to radicalise and attract new recruits and to inspire terrorist attacks around the world. In recent years we’ve seen the impact of this approach all over Europe, including attacks in London and Manchester. Daesh also used modern communications systems to command and control their battlefield operations. This allowed them to operate flexibly, at scale and pace, and to pose an even greater danger to the populations they sought to control, and to maximise their reach across their so-called caliphate. 

During the Battle of Mosul, the self-declared capital of Daesh, we used cyber tools and techniques in operations alongside the military in support of the coalition and as part of a wider, full-spectrum campaign. The outcomes of these operations were wide-ranging. Disruption of communications, degradation of propaganda, causing distrust within groups, and denial of equipment and networks used as part of their operations were all ways in which Daesh’s effectiveness could be reduced. We could also use cyber techniques to promote UK government messaging to targets, or to highlight their activities to those who may unsuspectingly be providing them with assistance. These operations made a significant contribution to coalition efforts to suppress Daesh propaganda, hindered their ability to coordinate attacks, and protected coalition forces on the battlefield.

<strong>Andrew, a member of the National Cyber Force</strong>

I’ve always been fascinated by the latest cutting-edge technology. Before working for the intelligence services, I joined the police and worked my way up from a Constable on the beat to specialising in digital forensics – looking at suspects’ electronic devices for evidence. I loved it but wanted to see what other opportunities were out there. 

I didn’t go to university after leaving school and my career so far has been led by a natural curiosity, and that’s true of all my colleagues who are joining the National Cyber Force. They come from all kinds of backgrounds. You have deep technical experts at the heart of it all, as well as a former supermarket branch manager, a primary school teacher and a fire fighter. The one thing we all have in common is an open mind, a hunger to learn and a shared goal of keeping the country safe – seeing both the threats and opportunities for national security from emerging technology. 

As a police officer I was immensely proud to help people on a personal level. Today, as part of this unique team at the National Cyber Force, I’m part of a force for good on a global scale.

###Delivering Our Ambition 

This strategy will mean nothing without a rigorous approach to implementing its objectives, monitoring and evaluating progress against them, and having the mechanisms to adjust course where necessary. This chapter sets out our approach to delivery.

####Roles and responsibilities across Government

The National Cyber Strategy will be one of the sub-strategies that will collectively deliver the ambitions of the Integrated Review. The National Security Council will exercise ministerial oversight of these strategies, monitoring implementation and considering the overall balance and direction of UK strategy. Progress against the objectives of the strategy will also be assessed via the Government Planning and Performance Framework and Outcome Delivery Plans.

All ministers play a role in ensuring that the UK cements its position as a responsible and democratic cyber power, able to protect and promote its interests in and through cyberspace. This list includes specific sets of responsibilities for ministers in leading roles, either for implementing or coordinating one or more of the five pillars of our National Cyber Strategy or overseeing our most important cyber capabilities and decisions. 

- <strong>The Chancellor of the Duchy of Lancaster</strong>, supported by the <strong>Paymaster General</strong>, provides overall leadership across departments to ensure an effective government response to cyber threats, and fulfilment of our ambitions as a cyber power. This includes the development and implementation of the National Cyber Strategy, the supporting programme of investment and coordination of the government’s efforts on cyber resilience. They also have overall cross-sector policy and coordination responsibility for the cyber security and resilience of the UK’s critical national infrastructure. The Chancellor of the Duchy of Lancaster is the default chair for ministerial COBR meetings on cyber incidents, when these are necessary. 

- <strong>The Secretary of State for the Home Department</strong> has a key role in the delivery of the National Cyber Strategy as a whole, and in the response to cyber incidents in line with their responsibilities for homeland security. They lead the government’s work to detect, disrupt and deter our adversaries alongside the Secretary of State for Foreign, Commonwealth and Development Affairs and the Secretary of State for Defence and provide overall coordination of that work. They also have specific responsibility to counter cyber crime. 

- <strong>The Secretary of State for Foreign, Commonwealth and Development Affairs</strong> has statutory responsibility for GCHQ and thus for the National Cyber Security Centre. They lead the government’s work to advance UK global leadership on cyber and have specific responsibility for the cyber attribution process, cyber sanctions regime and international engagement for high category cyber incidents. They also lead the government’s work to detect, disrupt and deter our adversaries alongside the Secretary of State for the Home Department and the Secretary of State for Defence.

- <strong>The Secretary of State for Defence</strong> leads the government’s work to detect, disrupt and deter our adversaries alongside the Secretary of State for Foreign, Commonwealth and Development Affairs and the Secretary of State for the Home Department.

- <strong>The Secretary of State for Foreign, Commonwealth and Development Affairs and the Secretary of State for Defence</strong> have responsibility for the National Cyber Force, as a joint endeavour between defence and intelligence.

- <strong>The Secretary of State for Digital, Culture, Media and Sport</strong> leads on the cyber security of organisations in the wider economy as it relates to digital policy, and the relevant growth, innovation and skills aspects of the National Cyber Strategy. They lead the government’s work to strengthen the UK’s cyber ecosystem and to take the lead in technologies vital to cyber power.

- <strong>Ministers of all Lead Government Departments for critical national infrastructure</strong> have responsibility for the cyber security and resilience policy for their sectors. 

- <strong>All ministers</strong> should provide oversight of the cyber security of their departments and implementation of appropriate mitigations. Where a department oversees an element of the public or private sector (for example DLUHC and local government or DEFRA and the water companies) they are responsible for the cyber policy and assurance activities relating to that sector. 

The Deputy National Security Adviser for Intelligence, Security and Resilience is the Senior Responsible Owner for the strategy and will lead delivery across government at official level, supported by the relevant senior officials across departments.

####Ministerial Responsibilities

(image)

####Investing in our cyber power

The government will be investing £2.6 billion in cyber and legacy IT over the next three years. This is in addition to significant investment in the National Cyber Force. It includes a £114 million increase in the National Cyber Security Programme with annual spending on capability built under the 2016 strategy moved under departmental management and put on a permanent footing. International programmes will be delivered via the Conflict, Stability and Security Fund (CSSF) to assist partner countries, build their cyber resilience and counter cyber threats. This is alongside increases in investment also announced in R&D, intelligence, defence, innovation, infrastructure and skills, all of which will contribute in part to the UK’s cyber power.<a href="CCS1021632420-002_National_Cyber_Strategy_005_Pillar_5.html#footnote-000">37</a>

####Measuring success

The strategy will be governed by a continuously evolving performance framework that reports to senior responsible officials and the National Security Council. The framework will be used to inform discussions with parliamentary and other bodies that oversee the national security community’s work. Consistent with the approach of the National Cyber Security Strategy 2016-2021, it will not be a public document due to the sensitive information contained but the government will publish annual progress reports.

This performance framework will:

- Provide a clear pathway of how activities will lead to the various objectives outlined in the strategy

- Ensure accountability for the delivery of the strategy

- Provide transparency on the progress the country is making towards the goals set out in the strategy

- Illustrate how activity needs to adapt to bring it in line with the strategy

- Understand what activities are effective in achieving strategic ambitions, so that these lessons can be applied in the future

- Provide a holistic view of activity across the five pillars, reducing duplication and identifying strengths and weaknesses within the country’s cyber power

- Ensure the strategy is providing cyber support to all sections of society

####Next steps

This strategy is intended as a guide for action, not only for those in government who have responsibility for cyber and the wide range of other related policies (see Annex A) but also for every person and organisation across the whole of our society with an interest in and responsibility for our national cyber effort. It is also the beginning of a conversation that we want to continue, to ensure our objectives and priorities remain relevant over the next five to ten years. We will use the publication of this strategy as a platform for further engagement with the public, private and third sectors across the UK and invite direct feedback to <a href="ukcyberstrategy@cabinetoffice.gov.uk">ukcyberstrategy@cabinetoffice.gov.uk</a>. We will report back annually on the progress we are making to implement this strategy. 